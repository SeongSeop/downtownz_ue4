// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "DowntownZGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class DOWNTOWNZ_API ADowntownZGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
